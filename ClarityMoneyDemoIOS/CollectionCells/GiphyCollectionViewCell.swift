//
//  GiphyCollectionViewCell.swift
//  ClarityMoneyDemoIOS
//
//  Created by Kwan Cheng on 4/6/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

import UIKit

class GiphyCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var imageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

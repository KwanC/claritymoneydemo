//
//  GiphyApi.swift
//  ClarityMoneyDemoIOS
//
//  Created by Kwan Cheng on 4/6/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

public enum GiphyServiceError : Error {
    case error(msg : String, detail: Error?)
}

public enum TrendingEvent {
    case progress(Double)
    case completed(Trending)
}

public class GiphyService {
    private enum EndPoints : String{
        case trending = "/v1/gifs/trending"
    }
    
    private let url : URL
    private let apiKey : String
    
    public let isLoading = Variable<Bool>(false)
    
    public init(url : URL, apiKey : String) {
        self.url = url
        self.apiKey = apiKey
    }
    
    public func rxTrending(limit : Int? = nil, rating: String? = nil) -> Observable<TrendingEvent> {
        return Observable<TrendingEvent>.create({ (subscriber) -> Disposable in
            self.isLoading.value = true
            var isDisposed = false

            let trendingUrl = self.url.appendingPathComponent(EndPoints.trending.rawValue)

            var parameters : Parameters = [String: Any]()
            parameters["api_key"] = self.apiKey
            if let limit = limit {
                parameters["limit"] = limit
            }
            
            var task : DataRequest?
            task = Alamofire.request(trendingUrl, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil)
                .downloadProgress(closure: { (progress) in
                    subscriber.onNext(TrendingEvent.progress(progress.fractionCompleted))
                })
                .validate()
                .responseJSON(completionHandler: { (response) in
                    if case let .failure(error) = response.result {
                        let theUrl = response.request?.url?.absoluteString ?? "Unknown URL"
                        let msg = "Trending API Error, to \(theUrl) see detail"
                        subscriber.onError(GiphyServiceError.error(msg: msg, detail: error))
                        return
                    }
                    
                    if isDisposed { return }
                    
                    guard let json = response.result.value as? [String:Any] else {
                        let msg = "Trending API Error, invalid json response received"
                        subscriber.onError(GiphyServiceError.error(msg: msg, detail: nil))
                        return
                    }
                    
                    guard let trendingResponse = Trending(json: json) else {
                        let msg = "Trending API Error, failed to create trending response from response."
                        subscriber.onError(GiphyServiceError.error(msg: msg, detail: nil))
                        return
                    }
                    
                    subscriber.onNext(TrendingEvent.completed(trendingResponse))
                    subscriber.onCompleted()
                    self.isLoading.value = false
                })
            
            return Disposables.create {
                isDisposed = true
                if let task = task {
                    task.cancel()
                }
            }
        })
    }
}

//
//  ViewController.swift
//  ClarityMoneyDemoIOS
//
//  Created by Kwan Cheng on 4/6/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Alamofire

class ViewController: UIViewController {
    @IBOutlet weak var collectionGifs: UICollectionView!
    @IBOutlet weak var btnGetMore: UIButton!
    @IBOutlet weak var indLoading: UIActivityIndicatorView!

    private var service : GiphyService!
    private var disposeBag : DisposeBag! = DisposeBag()
    fileprivate var trending : Trending?
    private var fetchLimit = 10
    
    let urlSession = URLSession(configuration: URLSessionConfiguration.default)
    let processQueue : OperationQueue = {
        let retQueue = OperationQueue()
        retQueue.name = "Image Process Queue"
        return retQueue
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let url = URL(string: "http://api.giphy.com/") else {
            // TODO: report error
            return
        }
        
        service = GiphyService(url: url, apiKey: "dc6zaTOxFJmzC")

        let cellNib = UINib(nibName: "GiphyCollectionViewCell", bundle: Bundle.main)
        collectionGifs.register(cellNib, forCellWithReuseIdentifier: "cell")
        
        let loadObservable = service.isLoading.asObservable()
        
        loadObservable.subscribe(onNext: { (isLoading) in
            self.btnGetMore.isEnabled = !isLoading
            self.collectionGifs.isUserInteractionEnabled = !isLoading
            self.collectionGifs.alpha = isLoading ? 0.5 : 1.0
            self.indLoading.isHidden = !isLoading
            if isLoading {
                self.indLoading.startAnimating()
            } else {
                self.indLoading.stopAnimating()
            }
        }).addDisposableTo(disposeBag)
        
        let btnGetMoreTapObserver = btnGetMore.rx.tap
        btnGetMoreTapObserver
            .flatMapLatest({ () -> Observable<TrendingEvent> in
                self.processQueue.cancelAllOperations()
                Alamofire.SessionManager.default.session.getAllTasks(completionHandler: { (tasks) in
                    tasks.forEach({$0.cancel()})
                })
                return self.service.rxTrending(limit: self.fetchLimit)
            })
            .subscribe(
                onNext: { (trendingEvent) in
                    guard case let .completed(trending) = trendingEvent else {
                        return
                    }
                    
                    self.performLoad(of: trending)
                },
                onError: { (error) in
                    let alert = UIAlertController(title: "Error Fetching Trending Gifs", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
            })
            .addDisposableTo(disposeBag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        btnGetMore.sendActions(for: UIControlEvents.touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func performLoad(of trending: Trending) {
        // change the limit
        self.fetchLimit = Int(arc4random_uniform(40) + 10)
        
        // queue and load up
        guard let data = trending.data else {
            // no data return without wiping out previous trending data
            return
        }
        
        self.trending = trending
        self.collectionGifs.reloadData()
        
        for i in 0 ..< data.count {
            let dataItem = data[i]
            guard let url = dataItem.images?.fixedHeightSmall?.url else {
                dataItem.loadState = DataLoadState.failed
                continue
            }
            
            Alamofire.request(url)
                .validate()
                .responseData(completionHandler: { (response) in
                    if case .failure(_) = response.result {
                        dataItem.loadState = DataLoadState.failed
                        return
                    }
                    
                    guard let data = response.result.value else {
                        dataItem.loadState = DataLoadState.failed
                        return
                    }
                    
                    let op = BlockOperation()
                    op.addExecutionBlock {
                        if op.isCancelled { return }
                        guard let image = UIImage(data: data) else {
                            dataItem.loadState = DataLoadState.failed
                            return
                        }
                        
                        dataItem.image = image
                        dataItem.loadState = DataLoadState.complete
                        let indexPath = IndexPath(row: i, section: 0)
                        
                        if !op.isCancelled {
                            DispatchQueue.main.async {
                                self.collectionGifs.reloadItems(at: [indexPath])
                            }
                        }
                    }
                    
                    self.processQueue.addOperation(op)
                })
        }
    }
}

extension ViewController : UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}

extension ViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let data = self.trending?.data else {
            return 0
        }
        
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? GiphyCollectionViewCell else {
            return collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        }
        
        guard let dataItem = self.trending?.data?[indexPath.row] else {
            return cell
        }

        switch dataItem.loadState {
        case .failed :
            cell.indicator.isHidden = true
        case .complete :
            cell.indicator.isHidden = true
            cell.imageView.image = dataItem.image
        case .loading :
            cell.indicator.isHidden = false
            cell.indicator.startAnimating()
            cell.imageView.image = nil
        }
        
        return cell
    }
}

//
//  ImagesResponse.swift
//  ClarityMoneyDemoIOS
//
//  Created by Kwan Cheng on 4/6/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

import Foundation
import Gloss

public class Images : Decodable {
    let fixedHeight : ImageProperties?
    let fixedHeightStill : ImageProperties?
    let fixedHeightDownsampled : ImageProperties?
    let fixedWidth : ImageProperties?
    let fixedWidthStill : ImageProperties?
    let fixedWidthDownsampled : ImageProperties?
    let fixedHeightSmall : ImageProperties?
    let fixedHeightSmall_still : ImageProperties?
    let fixedWidthSmall : ImageProperties?
    let fixedWidthSmallStill : ImageProperties?
    let downsized : ImageProperties?
    let downsizedStill : ImageProperties?
    let downsizedLarge : ImageProperties?
    let original : ImageProperties?
    let originalStill : ImageProperties?
    
    public required init?(json: JSON) {
        self.fixedHeight = "fixed_height" <~~ json
        self.fixedHeightStill = "fixed_height_still" <~~ json
        self.fixedHeightDownsampled = "fixed_height_downsampled" <~~ json
        self.fixedWidth = "fixed_width" <~~ json
        self.fixedWidthStill = "fixed_width_still" <~~ json
        self.fixedWidthDownsampled = "fixed_width_downsampled" <~~ json
        self.fixedHeightSmall = "fixed_height_small" <~~ json
        self.fixedHeightSmall_still = "fixed_height_small_still" <~~ json
        self.fixedWidthSmall = "fixed_width_small" <~~ json
        self.fixedWidthSmallStill = "fixed_width_small_still" <~~ json
        self.downsized = "downsized" <~~ json
        self.downsizedStill = "downsized_still" <~~ json
        self.downsizedLarge = "downsized_large" <~~ json
        self.original = "original" <~~ json
        self.originalStill = "original_still" <~~ json
    }
}

public class ImageProperties : Decodable {
    let url : String?
    let width : String?
    let height : String?
    let size : String?
    let frames : String?
    let mp4  : String?
    let mp4Size : String?
    let webp : String?
    let webpSize : String?
    
    public required init?(json: JSON) {
        self.url = "url" <~~ json
        self.width = "width" <~~ json
        self.height = "height" <~~ json
        self.size = "size" <~~ json
        self.frames = "frames" <~~ json
        self.mp4 = "mp4" <~~ json
        self.mp4Size = "mp4_size" <~~ json
        self.webp = "webp" <~~ json
        self.webpSize = "webp_size" <~~ json
    }
}

//
//  PaginationResponse.swift
//  ClarityMoneyDemoIOS
//
//  Created by Kwan Cheng on 4/6/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

import Foundation
import Gloss

public class Pagination: Decodable {
    let count : Int?
    let offset : Int?
    
    public required init?(json : JSON) {
        self.count = "count" <~~ json
        self.offset = "offset" <~~ json
    }
}

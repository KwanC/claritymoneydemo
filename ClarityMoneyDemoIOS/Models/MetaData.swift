//
//  MetaResponse.swift
//  ClarityMoneyDemoIOS
//
//  Created by Kwan Cheng on 4/6/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

import Foundation
import Gloss

public class MetaData : Decodable {
    let status : Int?
    let msg : String?
    
    public required init?(json : JSON) {
        self.status = "status" <~~ json
        self.msg = "msg" <~~ json
    }
}

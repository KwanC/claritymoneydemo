//
//  DataResponse.swift
//  ClarityMoneyDemoIOS
//
//  Created by Kwan Cheng on 4/6/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

import Foundation
import Gloss

public enum DataLoadState {
    case loading, complete, failed
}

public class DataItem : Decodable {
    let type : String?
    let id : String?
    let slug : String?
    let url : String?
    let bitlyGifUrl : String?
    let bitlyUrl : String?
    let embedUrl : String?
    let username : String?
    let source : String?
    let rating : String?
    let caption : String?
    let contentUrl : String?
    let sourceTld : String?
    let sourcePostUrl : String?
    let importDatetime : String?
    let trendingDatetime : String?
    let images : Images?
    
    var loadState = DataLoadState.loading
    var image : UIImage?
    
    public required init?(json: JSON) {
        self.type = "type" <~~ json
        self.id = "id" <~~ json
        self.slug = "slug" <~~ json
        self.url = "url" <~~ json
        self.bitlyGifUrl = "bitly_gif_url" <~~ json
        self.bitlyUrl = "bitly_url" <~~ json
        self.embedUrl = "embed_url" <~~ json
        self.username = "username" <~~ json
        self.source = "source" <~~ json
        self.rating = "rating" <~~ json
        self.caption = "caption" <~~ json
        self.contentUrl = "content_url" <~~ json
        self.sourceTld = "source_tld" <~~ json
        self.sourcePostUrl = "source_post_url" <~~ json
        self.importDatetime = "import_datetime" <~~ json
        self.trendingDatetime = "trending_datetime" <~~ json
        self.images = "images" <~~ json
    }
}

//
//  TrendingResponse.swift
//  ClarityMoneyDemoIOS
//
//  Created by Kwan Cheng on 4/6/17.
//  Copyright © 2017 Ghengis Kwan. All rights reserved.
//

import Foundation
import Gloss

public class Trending : Decodable {
    let data : [DataItem]?
    let pagination : Pagination?
    let meta : MetaData?
    
    public required init?(json: JSON) {
        self.data = "data" <~~ json
        self.pagination = "pagination" <~~ json
        self.meta = "meta" <~~ json
    }
}
